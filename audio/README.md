# Audio
You can open `software-containers-audio-mix.aup3` in [Audacity](https://www.audacityteam.org/).
It's a pretty simple audio mix: two dialog tracks, two pieces of music and a single sound effect (a [high five](https://freesound.org/people/maycuddlepie/sounds/330296/)). The main music piece ('Organs') is duplicated on two separate tracks: one unfiltered for when there is no dialog, and one with equalisation and filtering to remove some of the vocal range and centre-panned audio (to keep out of the way of the dialog). I've used Audacity's Envelope tool to fade between these tracks as needed.

# Music

The two tracks used in the animation are from [Maciej Kulesza](https://maciejkulesza.com/) & Ela Mazurkiewicz's album MADS, which I bought in 2017 but doesn't seem to be online anymore.

'Organs' has been spliced and lengthened to fit the animation.

Both tracks are licensed under Creative Commons Attribution.

[![MADS album cover](https://web.archive.org/web/20160308134928im_/https://f1.bcbits.com/img/a3519532754_2.jpg)](https://web.archive.org/web/20160224154648/http://madsmusic.bandcamp.com/album/mads-creativecommons)

[MADS album (Wayback Machine snapshot)](https://web.archive.org/web/20160224154648/http://madsmusic.bandcamp.com/album/mads-creativecommons)
