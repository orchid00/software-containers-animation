## Noun Project Icons

Many of the illustrations in this project were built upon icons & illustrations from [The Noun Project](https://thenounproject.com).

All are licensed under Creative Commons Attribution, though I chose to buy a NounPro membership in order to reduce the amount of attribution required in the video credits.

Full attribution can be found here though!
Most have been modified considerably for use in this animation.

[![scientist](scientist.svg) Scientist by Llisole](https://thenounproject.com/icon/1136188/)

[![doctor](doctor.svg) Doctor by Llisole](https://thenounproject.com/icon/2334579/)

[![brain](brain.svg) Brain by Oriol Sallés](https://thenounproject.com/icon/653780/)

[![gear](gear.svg) Gear by Alice Design](https://thenounproject.com/icon/2434664/)

[![python](python.svg) Python by Danil Polshin](https://thenounproject.com/icon/1375869/)

[![tree](tree.svg) Tree by Made by Made](https://thenounproject.com/icon/898845/)

[![truck](truck.svg) Truck by mikicon](https://thenounproject.com/icon/251790/)

[![train](train.svg) Train by Smalllike](https://thenounproject.com/icon/3447604/)

[![lego](lego.svg) Lego by Philipp Petzka](https://thenounproject.com/icon/3991138/)

[![image](image.svg) Image by Alfa Design ](https://thenounproject.com/icon/1709574/)
