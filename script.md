 **A:** Hey, I've been running my data analysis and I'm having some issues - my computer has just been updated, and now the program isn’t working!

**B:** Hmm, let's take a look - your program relies upon your computer system and some supporting software to properly work. I guess if they are changed, things are likely to break.

I've got an idea - I recently learned about something which could make your program more reliable _and_ make your analysis easily reproducible too. Now I use it all the time, and I think you can too.

**A:** What do you suggest?

**B:** Software Containers.

**A:** What are Software Containers?

**B:** Containers are packaged software instructions, they include everything needed to run a program, independently of the computer system. Depending on the program, that might include system settings, the programming language it's written in, or code for specific tasks like image processing or genome assembly. Software containers are named after shipping containers, as they have some similar features, particularly standardization and portability. Imagine you have a standard prepared software package that has everything you need to run your workflow and that you can use in any computer.

**A:** So I could use my software container with any operating system, like Windows, Mac or Linux?

**B:** Yes! I use containers all the time, because my containerised programs are easy to move from a laptop to another computer or a local or cloud server. If you need more power, you can even run your container on a supercomputer! Containers make programs portable.

**A:** Cool! I have a friend who wants to do a similar analysis to mine, but with datasets that span multiple years, maybe she can use this container too!

**B:** Yep! No need to waste time reinventing solutions.
For example, I have this program that generates plots from table data. Some of my collaborators had proprietary data that they couldn’t share with me, but I could share the container with them to produce their own plots on their computers.

**A:** I like these containers already, what else are they useful for?

**B:** Normally you might have one program that uses a particular version of some dependent software, and another that needs a different version. But since a container brings everything it needs with it, it avoids conflicting dependencies. You can run multiple containerised programs and they stay completely separate. Containers also add security to your system. The computer where the container runs is protected; as the container can only modify things inside itself.

**A:** So, containers allow me to reuse and to share my work across different systems, they improve the efficiency of my workflow, they make my analysis reproducible, and therefore more valuable..? I definitely need a container! How do I make one?

**B:** You _could_ make your own container, but first we can try to find a ready-to-use container with your software from the research computer center or in a global container repository.

**A:** That sounds like a great way to start!
