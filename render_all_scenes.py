# to render all scenes in the .blend file, run blender from the command line in this directory:
# blender software-containers-animation.blend -P render_all_scenes.py

import bpy
import os

# the renders folder path is relative to the blend file's directory
renders_folder = bpy.path.abspath("//renders")

for scene in bpy.data.scenes:
    scene.render.resolution_x = 3840
    scene.render.resolution_y = 2160
    scene.render.filepath = os.path.join(renders_folder, scene.name + "/" + scene.name)

for scene in bpy.data.scenes:
    if scene.name != '00_EDIT':
        bpy.ops.render.render(animation=True,  scene=scene.name)
