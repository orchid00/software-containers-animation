1
00:00:06,416 --> 00:00:09,041
Hey, I've been running my data analysis and

2
00:00:09,041 --> 00:00:10,250
I'm having some issues

3
00:00:10,250 --> 00:00:12,125
my computer has just been updated

4
00:00:12,125 --> 00:00:14,541
and now the program isn’t working

5
00:00:14,541 --> 00:00:16,333
Hmm, let's take a look

6
00:00:16,333 --> 00:00:18,916
your program relies upon your computer system

7
00:00:18,916 --> 00:00:22,375
and some supporting software to properly work

8
00:00:22,375 --> 00:00:23,833
I guess if they've changed

9
00:00:23,833 --> 00:00:25,541
things are likely to break

10
00:00:26,541 --> 00:00:27,916
I've got an idea

11
00:00:27,916 --> 00:00:29,416
I recently learned about something

12
00:00:29,416 --> 00:00:31,458
which could make your program more reliable

13
00:00:31,458 --> 00:00:34,791
and make your analysis easily reproducible too

14
00:00:34,791 --> 00:00:36,208
Now I use it all the time

15
00:00:36,208 --> 00:00:37,916
and I think you can too

16
00:00:37,916 --> 00:00:39,083
What do you suggest?

17
00:00:39,625 --> 00:00:40,958
Software Containers

18
00:00:40,958 --> 00:00:42,583
What are Software Containers?

19
00:00:42,583 --> 00:00:44,916
Containers are packaged software instructions

20
00:00:44,916 --> 00:00:47,583
they include everything needed to run a program

21
00:00:47,583 --> 00:00:49,750
independently of the computer system

22
00:00:49,750 --> 00:00:51,250
Depending on the program

23
00:00:51,250 --> 00:00:53,166
that might include system settings

24
00:00:53,166 --> 00:00:55,291
the programming language it's written in

25
00:00:55,291 --> 00:00:57,125
or code for specific tasks

26
00:00:57,125 --> 00:00:58,500
like image processing

27
00:00:58,500 --> 00:01:00,000
or genome assembly

28
00:01:00,916 --> 00:01:03,791
Software containers are named after shipping containers

29
00:01:03,791 --> 00:01:06,000
as they have some similar features

30
00:01:06,000 --> 00:01:08,500
particularly standardization and portability

31
00:01:09,416 --> 00:01:12,458
Imagine you have a standard prepared software package

32
00:01:12,458 --> 00:01:15,208
that has everything you need to run your workflow

33
00:01:15,208 --> 00:01:17,125
and that you can use in any computer

34
00:01:17,125 --> 00:01:19,250
So I could use my software container

35
00:01:19,250 --> 00:01:20,625
with any operating system

36
00:01:20,625 --> 00:01:22,625
like Windows, Mac or Linux?

37
00:01:22,625 --> 00:01:23,166
Yes!

38
00:01:23,166 --> 00:01:25,458
I use containers all the time

39
00:01:25,458 --> 00:01:27,666
because my containerised programs

40
00:01:27,666 --> 00:01:29,208
are easy to move from a laptop

41
00:01:29,208 --> 00:01:30,583
to another computer

42
00:01:30,583 --> 00:01:32,833
or a local or cloud server

43
00:01:33,791 --> 00:01:34,416
If you need more power

44
00:01:34,416 --> 00:01:35,750
you can even run your container

45
00:01:35,750 --> 00:01:37,083
on a supercomputer!

46
00:01:37,333 --> 00:01:39,291
Containers make programs portable

47
00:01:39,291 --> 00:01:40,166
Cool!

48
00:01:40,166 --> 00:01:41,666
I have a friend who wants to do

49
00:01:41,666 --> 00:01:43,166
a similar analysis to mine

50
00:01:43,166 --> 00:01:45,875
but with datasets that span multiple years

51
00:01:45,875 --> 00:01:48,750
maybe she can use this container too

52
00:01:48,750 --> 00:01:49,416
Yep!

53
00:01:49,416 --> 00:01:52,083
No need to waste time reinventing solutions

54
00:01:52,083 --> 00:01:54,708
For example, I have this program

55
00:01:54,708 --> 00:01:57,250
that generates plots from table data

56
00:01:57,250 --> 00:01:59,708
Some of my collaborators had proprietary data

57
00:01:59,708 --> 00:02:01,416
that they couldn’t share with me

58
00:02:01,416 --> 00:02:03,208
but I could share the container with them

59
00:02:03,208 --> 00:02:05,958
to produce their own plots on their computers

60
00:02:07,125 --> 00:02:08,625
I like these containers already

61
00:02:08,625 --> 00:02:10,000
what else are they useful for?

62
00:02:10,791 --> 00:02:12,958
Normally you might have one program

63
00:02:12,958 --> 00:02:16,083
that uses a particular version of some dependent software

64
00:02:16,083 --> 00:02:22,416
and another that needs a different version

65
00:02:19,666 --> 00:02:22,708
But since a container brings everything it needs with it

66
00:02:22,708 --> 00:02:25,208
it avoids conflicting dependencies

67
00:02:25,208 --> 00:02:27,875
You can run multiple containerised programs

68
00:02:27,875 --> 00:02:29,791
and they stay completely separate

69
00:02:30,500 --> 00:02:33,333
Containers also add security to your system

70
00:02:33,333 --> 00:02:36,208
The computer where the container runs is protected

71
00:02:36,208 --> 00:02:39,666
as the container can only modify things inside itself

72
00:02:39,666 --> 00:02:40,875
So, containers allow me

73
00:02:40,875 --> 00:02:44,083
to reuse and to share my work across different systems

74
00:02:44,083 --> 00:02:46,500
they improve the efficiency of my workflow

75
00:02:46,500 --> 00:02:48,583
they make my analysis reproducible

76
00:02:48,583 --> 00:02:50,333
and therefore more valuable..?

77
00:02:50,333 --> 00:02:52,416
I definitely need a container!

78
00:02:52,416 --> 00:02:53,416
How do I make one?

79
00:02:53,833 --> 00:02:55,750
You could make your own container

80
00:02:55,750 --> 00:02:57,458
following public recipes

81
00:02:57,458 --> 00:02:59,250
but first we can try to find

82
00:02:59,250 --> 00:03:01,333
a ready-to-use container with your software

83
00:03:01,333 --> 00:03:03,125
from the research computer centre

84
00:03:03,125 --> 00:03:05,541
or in a global container repository

85
00:03:05,541 --> 00:03:07,916
That sounds like a great way to start!

