# Software Containers Animation (ARDC)

[!['How Can Software Containers Help Your Research' on youtube](png/youtube-screenshot.png)](https://www.youtube.com/watch?v=HelrQnm3v4g)
[_'How Can Software Containers Help Your Research'_ on YouTube](https://www.youtube.com/watch?v=HelrQnm3v4g)

This animation is an introduction to the benefits of containers for research. It was produced for [Australian Research Data Commons](https://ardc.edu.au/), and developed in close collaboration with [Dr. Paula Martinez](https://twitter.com/orchid00).

## Contents
- [Video production](#video-production)
- [Production Software](#production-software)
- [CC-BY License](#cc-by-license)
- [How to use these source files](#how-to-use-these-source-files)
- [Help and Contact](#help-and-contact)


## Video production
- [Sam Muirhead (twitter)](https://twitter.com/cameralibre)
- [Camera Libre (website)](https://www.cameralibre.cc/)

## Production Software
Made with Free/Libre Open Source Software:
- [Blender](www.blender.org) ([Grease Pencil](https://www.blender.org/features/grease-pencil/))
- [Inkscape](inkscape.org)
- [Kdenlive](kdenlive.org)
- [Audacity](audacityteam.org)
- [Nextcloud](nextcloud.com) / [Jitsi](meet.jit.si) / [Git](https://git-scm.com/) for collaboration

## CC-BY License

Remixes, translations and adaptations of this animation are permitted under the terms of the [Creative Commons Attribution International (CC BY 4.0) ](https://creativecommons.org/licenses/by-sa/4.0/) licence.

This license covers the designs, script and animation, but does not cover:

- Any branding elements, including the ARDC, Nectar and NCRIS logos
- content supplied by third parties, such as voiceovers.

### How to attribute

The following attribution must be provided in the credits of any video adaptation of this animation:

**_'How can software containers help your research?'_ video, CC-BY Australian Research Data Commons, 2021.**

Preferably this attribution would provide some context of what you used or changed, for example:

- **Includes scenes from** _'How can software containers help your research?'_ video, CC-BY Australian Research Data Commons, 2021.

- **'Software Containers' slides based on designs from** _'How can software containers help your research?'_ video, CC-BY Australian Research Data Commons, 2021.

- **This video is a translation of** _'How can software containers help your research?'_ video, CC-BY Australian Research Data Commons, 2021.



Additionally, in your video description or website where the animation is displayed, you should link to the published original video and its Digital Object Identifier.

```
Australian Research Data Commons, 2021. How can software containers help your research?. [video]

Available at: https://www.youtube.com/watch?v=HelrQnm3v4g

DOI: http://doi.org/10.5281/zenodo.5091260
```

In other uses, such as slides, images etc, the above attribution should be (preferably) clearly displayed alongside the image, or in a credits / attribution section at the end of the document.

**NOTE:** A Creative Commons license is _just_ a copyright license - it does not cover rights beyond that such as trademark or moral rights.
You may not claim any association with, or the support of ARDC or its partners, and any adaptation must be clearly marked as distinct from the original, with clear attribution.
[Get in touch](#help-and-contact) if you're unsure, or if you'd like any guidance on this.

This project uses other CC-BY material.
Any re-use of designs based upon [these Noun Project icons](./svg/noun-project) or re-use of [music tracks](./music) should also attribute those original artists or arrange an attribution waiver with the artists for those works.

## How to use these source files

- First you'll need to **download** this repository, or clone it from the command line using `git clone`
```
git clone git@gitlab.com:cameralibre/software-containers-animation.git
```

- The **designs and layouts** for the animation are in the `svg` folder - these will work best when opened in [Inkscape](inkscape.org), but should work fine when opened in other vector programs like Affinity Designer or Adobe Illustrator.


- The **animation file** is `software-containers-animation.blend`. This includes every scene of the animation, animated with Grease Pencil, the 2D animation tool within [Blender](blender.org). Open the file with Blender and you can navigate between the different scenes using the scene dropdown menu near the top right.
If you're interested in working more with Grease Pencil, I recommend the tutorial ['Blender 2D/3D for Beginners'](https://www.youtube.com/watch?v=c57qq2nE3B0) by Dedouze, or Matias Mendiola's ['Grease Pencil Fundamentals'](https://cloud.blender.org/training/grease-pencil-fundamentals/).

- You'll probably want to **render** the image sequences on your own computer before you use the edit sequence (Scene `00_EDIT` in `software_containers_animation`). You can do that by using Blender's command line interface to run the script `render_all_scenes.py`. In your terminal, navigate to this folder and run:
```
blender software-containers-animation.blend -P render_all_scenes.py
```
Then go have lunch. This will take a while, especially if you don't have a particularly powerful computer. You'll see the progress of the render in blender's terminal output, or you can check in the `renders` folder in your file browser.

- Once all scenes are rendered, you can load the image sequences in the video editor of your choice, or use the video editor within Blender. You can automatically load the image sequences into the `00_EDIT` timeline using the script `import_image_sequences.py`:
```
blender software-containers-animation.blend -P import_image_sequences.py
```

## Help and contact

Feel free to reach out to me with any questions regarding the animation, its source files, the software, licensing requirements, or future projects. I'm happy to help!

You can contact me via my [website](cameralibre.cc), [twitter](twitter.com/cameralibre) or [fediverse](friend.camp/cameralibre), or by opening an [issue](https://gitlab.com/cameralibre/software-containers-animation/-/issues) in this repository.

### Contact ARDC
If you have questions about containers, contact the Australian Research Data Commons (ARDC):

- [Website](https://ardc.edu.au)
- [Twitter](https://twitter.com/ARDC_AU)
- [Linkedin](https://www.linkedin.com/company/australian-research-data-commons/)
- [YouTube](https://www.youtube.com/c/ARDC_AU)

Learn more about the [ARDC Nectar Research Cloud](http://ardc.edu.au/nectar)

If you are already familiar with containers in research, you are welcome to participate in the Australian Research Container Orchestration Services (ARCOS) Community supported by the ARDC. Visit the [ARCOS website](https://arcos.ardc.edu.au) to learn more.
